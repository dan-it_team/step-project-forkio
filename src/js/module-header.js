const btn = document.querySelector('.burger');
const menu = document.querySelector('.navbar__menu');

btn.onmouseover = () => {
    menu.style.display = 'block';
    setTimeout(function () {
       menu.style.opacity = '1';
    }, 500);
};
btn.onmouseout = () => {
    menu.style.display = 'none';
};